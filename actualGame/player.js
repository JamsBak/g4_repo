let yPos = 10; 
let xIncrement = -50;
let directionX; 
var jump = -15;
var gravity = 0.2;

class Player
{
    constructor(){
        this.sprite;
        this.bullet;
        this.enemy1;
        this.animations={
            standing:null,
            moving:null
        };
        this.bulletGroup;
        
    }

    p5Load(){ //call this in preload
        this.animations.standing=loadAnimation("sprites/mcSprite.png");
        this.bulletImg=loadImage("sprites/bullet.png");
        this.enemyImg = loadImage("sprites/enemySprite1.png");
    }

    addGroup()
    {
        this.bulletGroup = new Group();
    }
    
    spritePos()
    {
        this.sprite=createSprite(110,455);
        this.sprite.friction=0.1;
        this.sprite.debug=true;
        this.sprite.setCollider("rectangle", 0,0,30,100);
        this.sprite.addAnimation("standing",this.animations.standing)
        this.sprite.changeAnimation("standing");
    }

    update(){
        this.controls()
        if(int(this.sprite.velocity.x)==0){
            this.idle()
        }

    }
    idle(){
        this.sprite.changeAnimation("standing")
    }

    controls(){
        if(keyIsDown(LEFT_ARROW)) {
            this.sprite.changeAnimation("standing");
            this.sprite.mirrorX(-1)
            directionX = -1;
            this.sprite.velocity.x--
        }
        if(keyIsDown(RIGHT_ARROW)){
            this.sprite.changeAnimation("standing");
            this.sprite.mirrorX(1)
            directionX = 1;
            this.sprite.velocity.x++
        }
        if(keyWentDown(38)) //Jump
        {
            this.sprite.velocity.y = jump;
            jumpingSFX.play();
        
        }
           
        if(keyWentDown(32)) //Space bar for shoot
        {  
            if(directionX == 1)
            {
                this.bulletImg.resize(20,5);
                this.bullet = createSprite(this.sprite.position.x + 30,this.sprite.position.y- 15);
                this.bullet.addImage(this.bulletImg);
                this.bulletGroup.add(this.bullet);
                this.bullet.attractionPoint(5, enemy.enemy1.position.x, enemy.enemy1.position.y);
                this.bullet.life=100;
                gunshotSFX.play();
            }

            if(directionX == -1)
            {
                this.bullet = createSprite(this.sprite.position.x - 30,this.sprite.position.y - 15);
                this.bullet.addImage(this.bulletImg);
                this.bulletGroup.add(this.bullet);
                this.bullet.attractionPoint(5, enemy.enemy2.position.x, enemy.enemy2.position.y);
                this.bullet.life=100;
                gunshotSFX.play();
            } 
        
        }

        this.sprite.velocity.y+= gravity;

        this.bulletGroup.collide(enemy.enemyGroup, enemy.bulletExplosion); 
    }

}