let explosionArray = new Array();
let enemyCount = 1, spawn = 0;
let heart = new Array(5);
let lives = 5;
let totalScore = 0; 
class Enemy
{
    constructor(){
        this.enemy1;
        this.enemy2;
        this.explode; 
        this.explode2;
        this.enemyGroup; 
    }

    addGroup()
    {
        this.enemyGroup = new Group();
    }

    p5Load(){
        this.enemyImg = loadImage("sprites/enemySprite1.png");
        this.enemyImg2 = loadImage("sprites/weird thing.png");
        this.heartImg = loadImage("sprites/lifeHearts.png");
        this.font = loadFont("fonts/slkscr.ttf");
    }

    enemyPos(){
        this.enemyImg.resize(50,50);
        this.enemy1 = createSprite(700,420);
        this.enemy1.addImage(this.enemyImg);
        this.enemyGroup.add(this.enemy1);
    }

    enemyPos2(){
        this.enemyImg2.resize(70,70);
        this.enemy2 = createSprite(-1000,-50);
        this.enemy2.addImage(this.enemyImg2);
        this.enemyGroup.add(this.enemy2);
    }

    lives()
    {
        this.heartImg.resize(50,50);
        for(let i = 0; i < lives; i ++)
        {
            heart[i] = createSprite(xIncrement, 0);
            heart[i].addImage(this.heartImg);
            xIncrement += 30;
        }
    }

    update(){
        this.controls()
        for(let i = 0; i < explosionArray.length; i++)
        {
            if(explosionArray[i].animation.getFrame() == 5)                           //When Animation Frame Hits 6 (Last Frame of Animation) Remove Last Index of Animation Until It Dissapears
            {
            explosionArray[i].remove();
            }
        }
    } 
    

    enemySpawn()
    {
        if(spawn <= 5)
        {
        if(enemyCount == 0)
            {
            this.enemyImg.resize(50,50);
            this.enemy1 = createSprite(700,420);
            this.enemy1.addImage(this.enemyImg);
            this.enemyGroup.add(this.enemy1);
            this.enemy2 = createSprite(650, 380);
            this.enemyImg2.resize(70,70);
            this.enemy2.addImage(this.enemyImg2);
            this.enemy2.attractionPoint(2.5, player.sprite.position.x, player.sprite.position.y); 
            this.enemyGroup.add(this.enemy2);
            enemyCount = 2;
            spawn++; 
            }
        }

        if(spawn > 5 && spawn <= 10)
        {
        if(enemyCount == 0)
            {
            this.enemyImg.resize(50,50);
            this.enemy1 = createSprite(random(50,700),random(50,400));
            this.enemy1.addImage(this.enemyImg);
            this.enemyGroup.add(this.enemy1);
            this.enemy2 = createSprite(random(50, 700), random(50,400));
            this.enemyImg2.resize(70,70);
            this.enemy2.addImage(this.enemyImg2);
            this.enemy2.attractionPoint(2.5, player.sprite.position.x, player.sprite.position.y); 
            this.enemyGroup.add(this.enemy2);
            enemyCount = 2;
            spawn++; 
            }
        }

        if(spawn > 10 && spawn <= 20 && player.sprite.position.x > 700)
        {
        if(enemyCount == 0)
            {
            this.enemyImg.resize(50,50);
            this.enemy1 = createSprite(random(350,1500),random(50,400));
            this.enemy1.addImage(this.enemyImg);
            this.enemyGroup.add(this.enemy1);
            this.enemy2 = createSprite(random(350, 1500), random(50,400));
            this.enemyImg2.resize(70,70);
            this.enemy2.addImage(this.enemyImg2);
            this.enemy2.attractionPoint(2.5, player.sprite.position.x, player.sprite.position.y); 
            this.enemyGroup.add(this.enemy2);
            enemyCount = 2;
            spawn++; 
            }
        }

        }

    controls(){  /*not sure if this has to be different name so it doesnt get confused with the player controls*/

        this.enemy1.attractionPoint(0.05, player.sprite.position.x, player.sprite.position.y);
        this.enemyGroup.collide(player.sprite, this.explosion);
    } 

    totalScore()
    {
        textFont(this.font, 25);
        fill('white');
        text('Total Score: ' + totalScore, 325, 40);
    }

    explosion(spriteA, spriteB)
    {
        this.explode = createSprite(spriteA.position.x, spriteA.position.y);
        this.explode.debug = true;
        this.explode.addAnimation("sprites/explosion1.png","sprites/explosion2.png","sprites/explosion3.png","sprites/explosion4.png","sprites/explosion5.png","sprites/explosion6.png","sprites/explosion7.png");
        this.explode.scale = 2;
        this.explode.setCollider("rectangle", 0, 0, 10, 10);
        explosionArray.push(this.explode);
        this.explode.animation.looping = false;
        spriteA.remove();
        if(lives > 0)
        {
            heart[lives - 1].remove();
        }
        lives--;
        enemyCount = 0;
        explosionSFX.play();
    }

    gameOver()
    {
        if(lives == 0)
        {
            screen = 3;
        }
    }

    bulletExplosion(spriteA, spriteB)
    {
        this.explode = createSprite(spriteA.position.x, spriteA.position.y);
        this.explode.debug = true;
        this.explode.addAnimation("sprites/explosion1.png","sprites/explosion2.png","sprites/explosion3.png","sprites/explosion4.png","sprites/explosion5.png","sprites/explosion6.png","sprites/explosion7.png");
        this.explode.scale = 2;
        this.explode.setCollider("rectangle", 0, 0, 10, 10);
        explosionArray.push(this.explode);
        this.explode.animation.looping = false;
        spriteA.remove();
        spriteB.remove();
        totalScore += 200; 
        enemyCount = 0;
       //explosionSFX.play();
    }
}