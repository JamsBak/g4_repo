/**
 * A silly little class that handles all the logic for randomly picking between 1 of 6 colours and drawing it as the background
 * 
 * 
 */
 let tilesizeX = 70;
 let tilesizeY = 70; 

class Level{
    constructor(){
        this.floor;
        this.floor1;
        this.wall1;
        this.wall2;
        this.wall;
        this.mapFile;
        this.startImg;
        this.wallImg;
        this.command;
        this.tileType;
        this.tileGroup;
    }
    

    p5Load()
    {
    this.mapFile = loadStrings("data/map.txt");
    this.floorImg = loadImage("sprites/wall.png");
    this.startImg = loadImage("sprites/start.jpg");
    this.wallImg = loadImage("sprites/brick.png");
    }

    spriteResize(){
        this.wallImg.resize(100,60);
        this.floorImg.resize(100,60);
        this.startImg.resize(130,130);
    }

    addGroup()
    {
        this.tileGroup = new Group();
    }

    tiles()
    {

        this.floor1 = createSprite(0,0);
        this.floor1.addImage(this.floorImg);
 

    for (let i=0; i<this.mapFile.length;i++) // this loops through the txt file to pull the information from it
        { 
            this.command=(split(this.mapFile[i]," "));
            for (let y=0;y<this.mapFile[i].length;y++)
                {
                    this.tileType=this.command[y];
                    if(this.tileType=="0") // this will place the empty space for the player to move around in
                    {
                        this.floor = createSprite(y*width/this.mapFile.length,i*height/this.mapFile.length);
                        this.floor.addImage(this.floorImg);
                    }
                    if(this.tileType=="2") // this will place the walls
                    { 
                        this.wall1 = createSprite(y*(width/this.mapFile.length),-60);
                        this.wall1.addImage(this.wallImg);
                        this.wall2 = createSprite(-100, -60 + i*(height/this.mapFile.length));              //Made extra walls to fill in the blank 
                        this.wall2.addImage(this.wallImg);
                        this.wall = createSprite(y*(width/this.mapFile.length),i*(height/this.mapFile.length));
                        console.log(i*(height/this.mapFile.length));
                        this.wall.addImage(this.wallImg);
                        this.tileGroup.add(this.wall);
                    }
                }
        }
    
    }

    update()
    {
        this.controls();
    }

    controls()
    {
        player.sprite.collide(this.tileGroup); 
    }


}