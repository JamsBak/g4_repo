//this is the one you'll write your code in make sure to change the script tag in index.html to use main.js instead of test.js
let gameBackground, mainBackground;
let screen = 1;
let player = new Player();
let level = new Level();
let enemy = new Enemy();
  

function preload(){
    gameBackground = loadImage("img/gameBackground2.png");
    mainBackground = loadImage("img/bgimage1.png");

    mainFont = loadFont("fonts/slkscr.ttf");

    gunshotSFX = loadSound("sounds/gunshot.wav");
    gameOverSFX = loadSound("sounds/GameOver.wav");
    jumpingSFX = loadSound("sounds/playerjumping.wav");
    powerUpSFX = loadSound("sounds/powerup.wav");
    explosionSFX = loadSound("sounds/explode.wav");
    
    level.p5Load();
    player.p5Load();
    enemy.p5Load();   
}



function setup(){
    createCanvas(800,600);

    level.spriteResize();
    level.addGroup();
    level.tiles();
    player.addGroup();
    player.spritePos();
    enemy.addGroup();
    enemy.enemyPos();
    enemy.enemyPos2();

    camera.off();
    enemy.lives();
    camera.on();
}

function draw(){

    if(screen == 1){
        image((mainBackground), 0, 0, 800, 600);
        mainButton = createButton("Start Game");                                             //Create functional button that guides to instruction 
        mainButton.style('font-size', '40px');
        mainButton.style('font-family', 'slkscr');
        mainButton.style('background-color', 'purple');
        mainButton.style('color', 'white');
        mainButton.position(600, 400);
        mainButton.mousePressed(mainG); 
        
        creatorButton = createButton("Creators");                                             //Create functional button that guides to instruction 
        creatorButton.style('font-size', '20px');
        creatorButton.style('font-family', 'slkscr');
        creatorButton.style('background-color', 'white');
        creatorButton.style('color', 'purple');
        creatorButton.position(670, 450);
        creatorButton.mousePressed(creators); 
       
       textSize(45);
       textFont('slkscr');
       fill('white');
       text('metroid: super combat',95,250);
    }
    if(screen == 2){
       
        background('white');
        camera.on();
        camera.position.x = player.sprite.position.x + width/4;
        camera.position.y = width/3.4;
        camera.zoom=1;
        image(gameBackground, 0, 0, 800, 600);


        enemy.enemySpawn();
        player.update();
        level.update(); 
        enemy.update();
        drawSprites();
        stroke(255); 
        enemy.gameOver()

        camera.off();
        enemy.totalScore();
        pauseButton = createButton("Pause");                                             //Create functional button that guides to instruction 
        pauseButton.style('font-size', '30px');
        pauseButton.style('font-family', 'slkscr');
        pauseButton.style('background-color', 'white');
        pauseButton.style('color', 'purple');
        pauseButton.position(1000, 0);
        pauseButton.mousePressed(pauseMenu);
        camera.on();

        text('X: ' + mouseX, mouseX, mouseY);
        text('Y: ' + mouseY, mouseX + 100, mouseY);
    }
    if(screen == 3){
        camera.off();
        image((mainBackground), 0, 0, 800, 600);
        textSize(50);
        textFont('slkscr');
        fill('white');
        text('Game Over',220,250);
    }
    if(screen==4){
        camera.off();
        image((mainBackground), 0, 0, 800, 600);
        textSize(50);
        textFont('slkscr');
        fill('white');
        text('Creators', 260,70);
        textSize(30);
        text('Lydia Walker',281, 270);
        text('Nina Neilsen', 281, 320);
        text('Ella Bennett', 282, 370);

        mainmenuButton = createButton("Main Menu");                                             //Create functional button that guides to instruction 
        mainmenuButton.style('font-size', '30px');
        mainmenuButton.style('font-family', 'slkscr');
        mainmenuButton.style('background-color', 'white');
        mainmenuButton.style('color', 'purple');
        mainmenuButton.position(615, 510);
        mainmenuButton.mousePressed(mainMenu);
    }

    if(screen == 5){
        camera.off();
        image((mainBackground), 0, 0, 800, 600);
        textSize(70);
        textFont('slkscr');
        fill('white');
        text('Paused', 275, 300);
        textSize(30);

        backToGameButton = createButton("Go Back");                                             //Create functional button that guides to instruction 
        backToGameButton.style('font-size', '30px');
        backToGameButton.style('font-family', 'slkscr');
        backToGameButton.style('background-color', 'white');
        backToGameButton.style('color', 'purple');
        backToGameButton.position(640, 450);
        backToGameButton.mousePressed(mainG);

        backMainButton = createButton("Main Menu");                                             //Create functional button that guides to instruction 
        backMainButton.style('font-size', '30px');
        backMainButton.style('font-family', 'slkscr');
        backMainButton.style('background-color', 'white');
        backMainButton.style('color', 'purple');
        backMainButton.position(620, 510);
        backMainButton.mousePressed(mainMenu);

    }



}
function mainMenu(){
    screen = 1;
    removeElements();
}

function mainG(){
    screen = 2;
    removeElements();
}

function creators(){
    screen = 4;
    removeElements();
}

function pauseMenu()
{
    screen = 5;
    removeElements();
}