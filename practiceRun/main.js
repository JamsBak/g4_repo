//this is the one you'll write your code in make sure to change the script tag in index.html to use main.js instead of test.js
let level= new Level();
let player = new Player();

function preload(){
    level.p5Load()
    player.p5Load()
}

function setup(){
    level.p5Init()
    player.p5Init(width/2,height+250)
    createCanvas(400,400)
}

function draw(){
    level.update()
    player.update()
    if(player.sprite.collide(level.groundSprite)){
        //sirCheesalot is on the ground
    } else {
        player.sprite.velocity.y++
    }
    drawSprites()
}