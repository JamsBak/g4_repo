class Player{
    constructor(){
        this.sprite;
        this.animations={
            standing:null,
            moving:null
        };
    }
    p5Load(){ //call this in preload
        this.animations.standing=loadImage("img/sirCheesealot_standing.png")
        this.animations.moving=loadImage("img/sirCheesealot_moving.png")
    }
    p5Init(x=width/2,y=height/2){ //call this in set up
        this.sprite=createSprite(x,y)
        this.sprite.friction=0.1
        this.sprite.debug=true;
        this.sprite.addAnimation("standing",this.animations.standing)
        this.sprite.addAnimation("moving",this.animations.moving)
        this.sprite.changeAnimation("standing");
    }
    update(){
        this.controls()
        if(int(this.sprite.velocity.x)===0){
            this.idle()
        }
    }
    idle(){
        this.sprite.changeAnimation("standing")
    }
    controls(){
        if(keyIsDown(LEFT_ARROW)) {
            this.sprite.changeAnimation("moving");
            this.sprite.mirrorX(1)
            this.sprite.velocity.x--
        }
        if(keyIsDown(RIGHT_ARROW)){
            this.sprite.changeAnimation("moving");
            this.sprite.mirrorX(-1)
            this.sprite.velocity.x++
        }
    }
}